﻿namespace ConsoleApp7
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ATX atx = new ATX();
            RAM ram = new RAM();
            List<RAM> ramList = new List<RAM>();
            ramList.Add(new RAM() { Frequency = 5200});
            ramList.Add(new RAM() { Frequency = 5600});

            atx.СoincidenceCountRAM(ramList);
            atx.CoincidenceValuesOfFreq(ramList);
        }
    }
}